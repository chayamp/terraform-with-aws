provider "aws" {
  region = "us-east-1"
  access_key = "AKIA3XYKLYCCEX6RAHXW"
  secret_key = "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"
}

#1. create VPC
resource "aws_vpc" "prod" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "production"
  }
}
#2 create internet gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.prod.id

  tags = {
    Name = "production_gw"
  }
}
#3 create custom route table.
resource "aws_route_table" "prod-route-table" {
  vpc_id = aws_vpc.prod.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  route {
    ipv6_cidr_block        = "::/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "production-route"
  }
}

#4.create subnet
resource "aws_subnet" "subnet" {
    vpc_id = aws_vpc.prod.id
    cidr_block = "10.0.1.0/24"
    availability_zone = "us-east-1a"

    tags = {
      "Name" = "prod-subnet"
    }
  
}

#5 Assosciate subnet with Route table

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.subnet.id
  route_table_id = aws_route_table.prod-route-table.id
}

#6 create a security group to allow port 22,80,443
resource "aws_security_group" "allow_tls" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.prod.id

  ingress {
    description      = "HTTPS"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    #cidr_blocks      = [aws_vpc.main.cidr_block]
    #ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block]
    cidr_blocks      = ["0.0.0.0/0"]
  }
  ingress {
    description      = "HTTP"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    #cidr_blocks      = [aws_vpc.main.cidr_block]
    #ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block]
    cidr_blocks      = ["0.0.0.0/0"]
  }
  ingress {
    description      = "SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    #cidr_blocks      = [aws_vpc.main.cidr_block]
    #ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block]
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_web"
  }
}

#7 create network interface with an IP in the subnet which was created in step 4.
resource "aws_network_interface" "web-server" {
  subnet_id       = aws_subnet.subnet.id
  private_ips     = ["10.0.1.50"]
  security_groups = [aws_security_group.allow_tls.id]

#   attachment {
#     instance     = aws_instance.test.id
#     device_index = 1
#   }
}

#8 Assign an elastic IP to the network interface created in step 7
resource "aws_eip" "Elastic_ip" {
  vpc                       = true
  network_interface         = aws_network_interface.web-server.id
  associate_with_private_ip = "10.0.1.50"
  depends_on = [
    aws_internet_gateway.gw
  ]
}
output "outputs" {
  value = aws_eip.Elastic_ip.public_ip
}

#9 create ubuntu server and install/enable Apache2

resource "aws_instance" "web-server-instance" {
    ami = "ami-0557a15b87f6559cf"
    instance_type = "t2.micro"
    availability_zone = "us-east-1a"
    key_name = "mykey"

    network_interface {
      device_index = 0
      network_interface_id = aws_network_interface.web-server.id
    }

    user_data = <<-EOF
                #!/bin/bash
                sudo apt update -y
                sudo apt install apache2 -y
                sudo systemctl start apache2
                sudo bash -c 'echo first web server > /var/www/html/index.html'
                EOF
    tags ={
        Name = "web-server"
    }            
  
}